package main

import (
	"fmt"
	"os"
)

func cleanWorkspace(c config) {
	err := os.RemoveAll(c.projectName)
	if err != nil {
		fmt.Println("Error cleaning workspace: ", err)
		os.Exit(1)
	}
	fmt.Println("Workspace successfully cleared")
}
