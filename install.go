package main

import (
	"fmt"
	"log"
	"os/exec"
)

// Install Dependencies
func install(c config) {
	cmd := exec.Command(c.buildToolPath, c.installCmd)
	cmd.Dir = c.projectName
	log.Printf("Installing Dependencies...")
	output, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatal(err)
	}
	printOutput(output)
}

func printOutput(outs []byte) {
	if len(outs) > 0 {
		fmt.Printf("==> Output: %s\n", string(outs))
	}
}
