package main

type config struct {
	projectName   string
	projectType   string
	buildToolPath string
	installCmd    string
	testCmd       []string
}
