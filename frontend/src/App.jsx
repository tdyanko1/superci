import React from 'react';
import './App.css';

class App extends React.Component {
  render() {
      return (<Home />);
    }
}

class Home extends React.Component {
  state = {
      projects: []
  }
  componentDidMount() {
      fetch('http://localhost:8081/api/projects')
      .then(res => res.json())
      .then((data) => {
          this.setState({ projects: data})
          console.log(this.state.projects)
      })
      .catch(console.log)
  }

  render() {
    return (
      <div className="container">
          <h2>Projects</h2>
          {this.state.projects.map((project) => (
              <div className="card">
                  <div className="card-body">
                      <h5 className="card-title">{project.name}</h5>
                          <h6 className="card-subtitle">{project.type}</h6>
                  </div>
              </div>
          ))}
      </div>
    );
  }
}

export default App;
