package main

import (
	"net/http"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/gin-contrib/cors"
)


type Project struct {
	ID		int		`json:"id" binding:"required"`
	Name	string  `json:"name"`
	Type    string  `json:"type"`
}

var projects = []Project{
	Project{1, "MyNodeApp", "NodeJS"},
	Project{2, "MyGolangApp", "Go"},
}

func main() {
	router := gin.Default()
	router.Use(cors.Default(), static.Serve("/", static.LocalFile("./views", true)))
	api := router.Group("/api")
	{
		api.GET("/", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H {
				"message": "pong",
			})
		})
	}

	api.GET("/projects", ProjectHandler)
	// api.POST("/projects/new:projectID", NewProject)

router.Run(":8081")
}

// Retrieves list of available projects
func ProjectHandler(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.JSON(http.StatusOK, projects)
}

// Creates a new project
func NewProject(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.JSON(http.StatusOK, gin.H {
		"message":"NewProject handler not implemented yet",
	})
}









// Typical CI functions we will implement later once the server is running
// config := config{
// 	projectName:   "strider",
// 	projectType:   "nodejs",
// 	buildToolPath: "/home/terry/.nvm/versions/node/v9.8.0/bin/npm",
// 	installCmd:    "install",
// 	testCmd:       []string{"run", "test"},
// }
// cleanWorkspace(config)
// checkout(config)
// install(config)
// runTests(config)
// cleanWorkspace(config)
