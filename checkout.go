package main

import (
	"fmt"
	"log"
	"os"

	git "gopkg.in/src-d/go-git.v4"
)

// Clone in the project repo
func checkout(c config) {
	fmt.Println("checking out project...")

	_, err := git.PlainClone(c.projectName, false, &git.CloneOptions{
		URL:      "https://github.com/tyanko1/strider",
		Progress: os.Stdout,
	})
	if err != nil {
		log.Fatal(err)
	}
}
