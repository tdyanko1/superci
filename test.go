package main

import (
	"log"
	"os/exec"
)

// Run the test suite(s)
func runTests(c config) {
	cmd := exec.Command(c.buildToolPath, c.testCmd[0], c.testCmd[1])
	// For the above, we need to figure out how to pass in a dynamic number of arguments
	// for the test command based on the number of elements in the testCmd slice
	cmd.Dir = c.projectName
	log.Printf("Executing test suite...")
	output, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatal(err)
	}
	printOutput(output)
}
