# SuperCI

## Mission
The intention of this project is to build a continuous integration server that is lean, fast, reliable, extensible, and built with user experience in mind. 

## Initial objectives
Here is a list of some of the basic functionality that most folks would expect ouf of a CI server that we can start with. This would be stage 1:

1. User configures the CI server with their project details including the Project Name, type (nodejs, java, ruby, python, etc.), and other relevant details
2. CI server checks out the user's project from source control (wherever it may be hosted) and executes a the CI pipeline which would look something like the following:
    * download git project into a local dir
    * install the build tools and languages needed to build the project as specified by the user 
    * run the user's build command and provide streaming log output so the job progress can be monitored 
    * install the user's dependencies 
    * run the user's test commands(s), pass the build if the tests pass, fail the build if the tests fail
    * Provide feedback to the user as to the whether the build was successful and cleanup the workspace.

We can start with building out the core functionality locally before we dig into putting together a full blown rest API, and front end UI. That will be stage 2 

## Toolset

Tools we will be using:
* **Lanugage:** Go
* **Test Framework:** Go built in test suite (for now)
* **Recommended IDE:** Visual Studio Code (with Go Packages installed)

## Current state
As of now I have an extremely lean skeleton in place. That performs most of the functions mentioned above at a **very** basic level. Get creative and implement whatever features you think would be cool. 

## Vision

The grand vision for this project looks something like this: 

* Visually pleasing, intuitive interface, broad support for different types of applications
* Super fast!
* Native Integrations with AWS
* Built in dashboard that provides provides useful metrics related to build status, git repo activity, delivery pipeline activity, code quality (could possibly integrate with SonarQube), and other useful business metrics. This could be our differentiating feature. Think of it as a CI server and Enterprise DevOps dashboard / radiator all wrapped up in one delightful package! 

![dashboard](http://capitalone.github.io/Hygieia/media/images/Screenshots/hygiea-screenshot.jpg)

* People will want to use our CI server is because it is fun and pleasing to interact with, it's easy to use, and streamlines their organizations workflow.
